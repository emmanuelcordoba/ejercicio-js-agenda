class Contacto {

    constructor(nombre, telefono) {
        this.nombre = nombre;
        this.telefono = telefono;
    }

    set setNombre(nombre) {
        this.nombre = nombre;
    }

    get getNombre() {
        return this.nombre;
    }

    set setTelefono(telefono) {
        this.telefono = telefono;
    }

    get getTelefono() {
        return this.telefono;
    }
}


function agregarContacto() {
    let nombre = document.getElementById('nombre');
    let telefono = document.getElementById('telefono');

    //Verifico que estén todos los campos
    if (nombre.value != "" && telefono.value != "") {
        let nuevoContacto = new Contacto(
            nombre.value,
            telefono.value
        );

        if(existeContacto(nuevoContacto)){
            alert('Ya existe un contacto con ese nombre');
        } else {
            let agenda = JSON.parse(localStorage.getItem('agenda'));
            if (!agenda) {
                agenda = [];
            }
            agenda.push(nuevoContacto);
            localStorage.setItem('agenda', JSON.stringify(agenda));
    
            //Limpio el formulario
            nombre.value = "";
            telefono.value = "";
            alert('Contacto guardado correctamente');
        }

    } else {
        alert('Campos incompletos!!');
    }
}

function listarContactos(agenda = null) {
    if(agenda == null){
        agenda = JSON.parse(localStorage.getItem('agenda'));
        //Controlo si tengo la agenda vacía
        if (!agenda) {
            agenda = [];
        }
    }

    //Genero el contenido de la tabla
    let tabla = "";
    for (let index = 0; index < agenda.length; index++) {
        let contacto = agenda[index];
        tabla += '<tr><td>' + contacto.nombre + '</td><td>' + contacto.telefono + '</td>';
        tabla += '<td><button type="button" class="btn btn-warning btn-sm mr-1" data-toggle="modal" data-target="#editarContactoModal" onclick="cargarFormMod(\''+contacto.nombre+'\')">Modificar</button>';
        tabla += '<button type="button" class="btn btn-danger btn-sm" onclick="eliminarContacto(\''+contacto.nombre+'\')">Eliminar</button></td></tr>';
    }
    //Muestro el contenido de la tabla
    document.getElementById('tablaContactos').innerHTML = tabla;
}

function eliminarContacto(nombre) {
    if(confirm('¿Esta seguro de borrar el contacto?')){
        let agenda = JSON.parse(localStorage.getItem('agenda'));
        let indice = agenda.findIndex(cont => cont.nombre == nombre);
        agenda.splice(indice,1);
        localStorage.setItem('agenda', JSON.stringify(agenda));
        alert('Contacto eliminado');
        //listarContactos();
        buscar();
    }
}

function existeContacto(contact) {
    let agenda = JSON.parse(localStorage.getItem('agenda'));
    if(agenda){
        return agenda.find(contacto => contacto.nombre == contact.nombre) != null;
    } else {
        return false;
    }
}

function buscar() {
    console.log(event);
    if (event.type == "submit") {
        event.preventDefault();
    }
    let txt = document.getElementById('txt-buscador').value;
    let agenda = JSON.parse(localStorage.getItem('agenda'));
    // Controlo que se haya ingresado algo en el buscador
    // y que la agenda no este vacía
    if (txt && agenda) {
        let agendaFiltada = agenda.filter(contacto => contacto.nombre.toLowerCase().indexOf(txt.toLowerCase()) > -1 );
        listarContactos(agendaFiltada);
    } else {
        listarContactos(agenda);
    }
}

function cargarFormMod(name){
    let agenda = JSON.parse(localStorage.getItem('agenda'));
    let contact = agenda.find(c => c.nombre == name);
    
    document.getElementById('nombre-modal').value = contact.nombre
    document.getElementById('telefono-modal').value = contact.telefono
}

function modificar(){
    let nombre = document.getElementById('nombre-modal').value;
    let newTelefono = document.getElementById('telefono-modal').value;
    if(newTelefono != ''){
        let agenda = JSON.parse(localStorage.getItem('agenda'));
        let index = agenda.findIndex(c => c.nombre == nombre);
        agenda[index].telefono = newTelefono;
        localStorage.setItem('agenda', JSON.stringify(agenda));
        alert('Contacto actualizado');
        $('#editarContactoModal').modal('toggle');
        buscar();
    } else {
        alert('Ingrese número de telefono');
    }

}
